package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Adaptater implements ISensor {
	private LegacyTemperatureSensor sensor;
	
	public Adaptater() {
		sensor = new LegacyTemperatureSensor();
	}

	/**
     * Enable the sensor.
     */
    public void on() {
    	if(!this.sensor.getStatus()) {
    		sensor.onOff();
    	}
    }

    /**
     * Disable the sensor.
     */
    public void off() {
    	if(sensor.getStatus()) {
    		sensor.onOff();
    	}
    }

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus() {
    	return sensor.getStatus();
    }

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() {
    	return sensor.getTemperature();
    }

	@Override
	public void update() throws SensorNotActivatedException {		
	}
}
