package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorOff implements ISensor {

	boolean status;
	double val = 0;

	SensorState SensorState;

	public SensorOff(SensorState SensorState) {
		this.SensorState = SensorState;
		this.val = SensorState.value;
		this.status = SensorState.status;
	}

	@Override
	public void on() {
		status = true;
		SensorState.setSensorState(SensorState.getOnState());
	}

	@Override
	public void off() {
		status = false;
		SensorState.setSensorState(SensorState.getOffState());
	}

	@Override
	public boolean getStatus() {
		return status;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException(
				"Sensor must be activated before acquiring new vals.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException(
				"Sensor must be activated to get its value.");
	}
}
