package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

abstract class DecorateurTemp implements ISensor {
	
	protected ISensor sensor;
	
	public DecorateurTemp(ISensor newSensor) {
		sensor = newSensor;
	}
	
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue();
	}

}
