package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorState {

	boolean status;
	double value;

	ISensor off;
	ISensor on;

	ISensor SensorState;

	public SensorState() {

		off = new SensorOff(this);
		on = new SensorOn(this);

		if (status == true)
			SensorState = on;
		else {
			SensorState = off;
		}
	}

	void setSensorState(ISensor newTempetatureSensor) {
		SensorState = newTempetatureSensor;

	}

	public void on() {
		SensorState.on();
	}

	public void off() {
		SensorState.off();
	}

	public void getStatus() {
		SensorState.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		SensorState.update();
	}

	public void getValue() throws SensorNotActivatedException {
		SensorState.getValue();
	}

	public ISensor getOnState() {
		return on;
	}

	public ISensor getOffState() {

		return off;

	}

	public void setValue(double value) {
		this.value = value;
	}
}
