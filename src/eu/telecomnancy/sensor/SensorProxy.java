package eu.telecomnancy.sensor;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:07
 */
public class SensorProxy implements ISensor {
	static Date date;
	DateFormat mediumDateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);

    private ISensor sensor;
    private SensorLogger log;

    public SensorProxy(ISensor _sensor, SensorLogger sensorLogger) {
        sensor = _sensor;
        log = sensorLogger;
    }

    @Override
    public void on() {
        log.log(LogLevel.INFO, "Sensor On");
        sensor.on();
        date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : m�thode on() : " + sensor.getStatus());

    }

    @Override
    public void off() {
        log.log(LogLevel.INFO, "Sensor Off");
        sensor.off();
        date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : m�thode off() : " + sensor.getStatus());

    }

    @Override
    public boolean getStatus() {
    	date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : m�thode getStatus() : " + sensor.getStatus());
        log.log(LogLevel.INFO, "Sensor getStatus");
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : m�thode update() : " + sensor.getValue());
        log.log(LogLevel.INFO, "Sensor update");
        sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	date = new Date();
        System.out.println(mediumDateFormat.format(date) + " : m�thode update() : " + sensor.getValue());
        log.log(LogLevel.INFO, "Sensor value =" + sensor.getValue());
        return sensor.getValue();
    }
}
