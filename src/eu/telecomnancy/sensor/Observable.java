package eu.telecomnancy.sensor;
import java.util.*;

public abstract class Observable{

	private ArrayList<Observer> obsList =  new ArrayList<>();	

	public void addObserver(Observer o) {
		obsList.add(o);
	}

	public void removeObserver(Observer o) {
		obsList.remove(o);
	}

	public void notifyObservers() throws SensorNotActivatedException {
		for(int i=0; i<obsList.size(); i++){			
			obsList.get(i).update();
		}
	}




}
