package eu.telecomnancy.sensor;

public class Off implements ICommande {

	private TemperatureSensor sensor;

	public Off(TemperatureSensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		sensor.off();
	}
}
