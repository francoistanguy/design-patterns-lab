package eu.telecomnancy.sensor;

public class On implements ICommande {

	private TemperatureSensor sensor;

	public On(TemperatureSensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		sensor.on();
	}

}
