package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Arondi extends DecorateurTemp {

	public Arondi(ISensor sens) {
		super(sens);
	}
	
	public double getValue() throws SensorNotActivatedException {
		double r = Math.round(sensor.getValue() * 100) ; // x100 /100 => donne 2 decimal (car 2 zeros)
		r = r/100;
        return r;
	}

	@Override
	public void on() {
		sensor.on();
	}

	@Override
	public void off() {
		sensor.off();		
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
		
	}
}
