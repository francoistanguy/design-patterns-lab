package eu.telecomnancy;

import java.io.IOException;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;

public class FactoryTest {
    public static void main(String[] args) throws IOException{
        Factory f = new Factory();
        ISensor s = f.getSensor();
        new ConsoleUI(s);
    }
}



