package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Fahrenheit extends DecorateurTemp{
	
	public Fahrenheit(ISensor sens) {
		super(sens);
	}
	
	public double getValue() throws SensorNotActivatedException {
		return 1.8*sensor.getValue()+32;
	}

	@Override
	public void on() {
		sensor.on();
	}

	@Override
	public void off() {
		sensor.off();		
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
		
	}
}
