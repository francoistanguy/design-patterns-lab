package eu.telecomnancy;

import java.util.Random;
import eu.telecomnancy.sensor.ISensor;

public class SensorOn implements ISensor {
	boolean status;
	double val = 0;

	SensorState SensorState;

	public SensorOn(SensorState SensorState) {
		this.SensorState = SensorState;
		this.val = SensorState.value;
		this.status = SensorState.status;
	}

	@Override
	public void on() {
		status = true;
		SensorState.setSensorState(SensorState.getOnState());
	}

	@Override
	public void off() {
		status = false;
		SensorState.setSensorState(SensorState.getOffState());
	}

	@Override
	public boolean getStatus() {
		return status;
	}

	@Override
	public void update() {
		val = (new Random()).nextDouble() * 100;

	}

	@Override
	public double getValue() {

		return val;

	}
}
