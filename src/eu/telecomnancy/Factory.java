package eu.telecomnancy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.Adaptater;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.sensor.TemperatureSensor;

public class Factory extends SensorFactory{
    ArrayList<ISensor> list = new ArrayList<ISensor>();
    
    public Factory() throws IOException{
        
        ReadPropertyFile rp = new ReadPropertyFile();
        Properties p = rp.readFile("/eu/telecomnancy/sensor.properties");
        for (String i: p.stringPropertyNames()) {
            if(p.getProperty(i).equals("TemperatureSensor")){
                this.list.add(new TemperatureSensor());
            } else if(p.getProperty(i).equals("LegacyTemperatureSensor")){
                this.list.add(new Adaptater());
            } else{
                System.out.println("erreur de saisie a la ligne "+i);
            }
        }
    }


    @Override
    public ISensor getSensor() {
        return this.list.get(0);
    }

}
